package at.bitfire.icsdroid.ui

import androidx.appcompat.app.AppCompatActivity

interface StartupFragment {

    fun initialize(activity: AppCompatActivity)

}